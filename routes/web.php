<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// Homepage, here we'll redirect unauthorized requests
Route::get('/', function ()
{
    return view('welcome');
});

// Authorization routes (login, logout, register (disabled)...)
Auth::routes();

// Dashboard routes
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::get('/dashboard_admin', 'DashboardAdminController@index')->name('dashboard_admin');

/**
 * Le rotte per gli oggetti sono composte dalle 7 rotte RESTFUL + 2 nuove:
 * /register (create/store) => Crea una nuova entità
 * /layout (layout) => Mostra lo schema dell'entità (NUOVA)
 * /search (index) => Mostra un elenco di entità (in base al filtro di ricerca)
 * /{id} (show) => Mostra un'entità per intero
 * /{id}/snippet (snippet) => Mostra un riassunto dell'entità (NUOVA)
 * /{id}/edit (edit/update) => Aggiorna un'entità
 * /{id}/delete => elimina un'entità
 */
// Client routes
Route::get('/clients/register', 'ClientController@create')->name('register_client');
Route::post('/clients/register', 'ClientController@store');
Route::get('/clients/layout', 'ClientController@layout');
Route::get('/clients/search', 'ClientController@index');
Route::get('/clients/{id}', 'ClientController@show')->name('client');
Route::get('/clients/{id}/snippet', 'ClientController@snippet')->name('snippet_client');
Route::get('/clients/{id}/edit', 'ClientController@edit')->name('edit_client');
Route::patch('/clients/{id}/edit', 'ClientController@update');
Route::delete('/clients/{id}/delete', 'ClientController@destroy')->name('delete_client');

// Project routes
Route::get('/projects/register', 'ProjectController@create')->name('register_project');
Route::post('/projects/register', 'ProjectController@store');
Route::get('/projects/layout', 'ProjectController@layout');
Route::get('/projects/search', 'ProjectController@index');
Route::get('/projects/{id}', 'ProjectController@show')->name('project');
Route::get('/projects/{id}/snippet', 'ProjectController@snippet')->name('snippet_project');
Route::get('/projects/{id}/edit', 'ProjectController@edit')->name('edit_project');
Route::patch('/projects/{id}/edit', 'ProjectController@update');
Route::delete('/projects/{id}/delete', 'ProjectController@destroy')->name('delete_project');

// User routes
Route::post('/users/register', 'Auth\RegisterController@register')->name('register_user');
Route::get('/users/search', 'UserController@index');
Route::get('/users/layout', 'UserController@layout');
Route::get('/users/{id}', 'UserController@show')->name('user');
Route::get('/users/{id}/snippet', 'UserController@snippet')->name('snippet_user');
Route::get('/users/{id}/edit', 'UserController@edit')->name('edit_user');
Route::patch('/users/{id}/edit', 'UserController@update');
Route::delete('/users/{id}/delete', 'UserController@destroy')->name('delete_user');

// Project-User
Route::get('/projectusers/register', 'ProjectUserController@create')->name('register_projectuser');
Route::post('/projectusers/register', 'ProjectUserController@store');

//Scheda ore
Route::get('/hours/register', 'HourController@create')->name('register_hour');
Route::post('/hours/register', 'HourController@store');
