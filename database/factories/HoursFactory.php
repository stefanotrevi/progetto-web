<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Hours;
use App\ProjectUser;
use Faker\Generator as Faker;

$factory->define(Hours::class, function (Faker $faker)
{
    $won = ProjectUser::all()->random();
    $proj = $won->project;

    return [
        'user_id' => $won['user_id'],
        'project_id' => $won['project_id'],
        'data' => $faker->dateTimeBetween($proj['data_inizio'], $proj['data_fine']),
        'ore' => $faker->numberBetween(1, 12),
        'note' => $faker->paragraph,
    ];
});
