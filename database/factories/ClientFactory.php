<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Client;
use Faker\Generator as Faker;
use Faker\Provider\it_IT as Provider;

$factory->define(Client::class, function (Faker $faker)
{
    $faker->addProvider(new Provider\Company($faker));
    $faker->addProvider(new Provider\Person($faker));

    return [
        'ragione' => $faker->company,
        'referente' => $faker->name,
        'email' => $faker->companyEmail,
        'ssid' => $faker->taxId(),
        'pec' => $faker->safeEmail,
        'iva' => $faker->unique()->vatId(),
    ];
});
