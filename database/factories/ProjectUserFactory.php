<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ProjectUser;
use App\Project;
use App\User;
use Faker\Generator as Faker;

$factory->define(ProjectUser::class, function (Faker $faker)
{
    do
    {
        $proj = Project::all()->random();
        $user = User::all()->random();
    } while (ProjectUser::query()
        ->where('user_id', '=', $user['id'])
        ->where('project_id', '=', $proj['id'])->count() > 0);

    return [
        'project_id' => $proj['id'],
        'user_id' => $user['id'],
    ];
});
