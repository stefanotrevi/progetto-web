<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Project;
use Faker\Generator as Faker;
use Faker\Provider\it_IT as Provider;

$factory->define(Project::class, function (Faker $faker)
{
    $faker->addProvider(new Provider\Company($faker));
    $faker->addProvider(new Provider\Person($faker));
    $start = $faker->dateTimeBetween('-2 years', '+1 years');

    return [
        'nome' => $faker->words(3, true),
        'descrizione' => $faker->paragraph,
        'note' => $faker->paragraph,
        'data_inizio' => $start,
        'data_fine' => $faker->dateTimeBetween($start, '+1 years'),
        'client_id' => \App\Client::all()->random()['id'],
        'costo_orario' => (double)$faker->numberBetween(99, 2000) / 100,
    ];
});
