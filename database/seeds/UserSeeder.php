<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->
        insert([
                   'name' => 'admin',
                   'email' => 'admin@gmail.com',
                   'password' => Hash::make('ciaociao'),
               ]);
        DB::table('users')->
        insert([
                   'name' => 'Lorenzo',
                   'email' => 'lore.cervi@gmail.com',
                   'password' => Hash::make(Str::random()),
               ]);
        factory(App\User::class, 10)->create();
    }
}
