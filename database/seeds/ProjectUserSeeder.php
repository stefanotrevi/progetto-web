<?php

use Illuminate\Database\Seeder;

class ProjectUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // altrimenti vengono generate in parallelo (entry duplicate)
        for ($fact = factory(App\ProjectUser::class, 1), $i = 0; $i < 100; ++$i)
            $fact->create();
    }
}
