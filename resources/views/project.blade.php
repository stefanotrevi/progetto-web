@php
    $id = $id ?? 'NULL';
    $data = $data ?? ['nome' => ''];
    $i = $tot_hrs = $tot_mny = 0;
    $users = $users ?? [];
    $prest = $prest ?? [];
@endphp
@extends('layouts.app')
@section('content')
    <div class="mt-5 container text-center">
        <h1>{{$data['nome']}}</h1>

        <div class="accordion mb-5 mt-4">
            @include('layouts.entity_register',
                    ['entity' => 'project', 'kind' => 'edit', 'extended' => '', 'title' => 'Modifica Progetto'])
            @include('layouts.entity_register',
                    ['entity' => 'projectuser', 'title' => 'Assegna un utente al progetto'])
        </div>

        @include('layouts.date_selector', ['route' => 'project'])
        <table class="table table-striped table-bordered mt-4">
            <thead class="thead-dark">
            <tr>
                <th>Utente</th>
                <th>Prima prestazione</th>
                <th>Ultima prestazione</th>
                <th>Ore spese</th>
                <th>Spesa</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $u)
                <tr>
                    <td>{{$u['name']}}</td>
                    <td>{{$prest[$i]['first']}}</td>
                    <td>{{$prest[$i]['last']}}</td>
                    <td>{{$hours[$i]}}h</td>
                    <td>€{{$hours[$i] * $data['costo_orario']}}</td>
                </tr>
                @php $tot_hrs += $hours[$i]; $tot_mny +=  $hours[$i++] * $data['costo_orario']; @endphp
            @endforeach
            @if($i > 1)
                <tr>
                    <th colspan="3">Totale</th>
                    <th>{{$tot_hrs}}h</th>
                    <th>€{{$tot_mny}}</th>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
    <div class="emptyspace">
    </div>
@stop
