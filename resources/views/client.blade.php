@php
    $id = $id ?? '';
    $data = $data ?? [];
    $projects = $projects ?? [];
    $hours = $hours ?? [];
    $i = $tot_hrs = $tot_mny = 0;
@endphp

@extends('layouts.app')
@section('content')
    <div class="mt-5 container text-center">
        <h1>{{$data['ragione'] ?? ''}}</h1>

        @include('layouts.form_base', ['entity' => 'client', 'kind' => 'edit'])

        @include('layouts.date_selector', ['route' => 'client'])

        <table class="table table-striped table-bordered mt-4">
            <thead class="thead-dark">
            <tr>
                <th>Progetto</th>
                <th>Data Inizio</th>
                <th>Data Fine</th>
                <th>Costo orario</th>
                <th>Ore spese</th>
                <th>Spesa totale</th>
            </tr>
            </thead>
            <tbody>
            @foreach($projects as $p)
                <tr>
                    <td><a href="{{route('project', ['id' => $p['id']])}}">{{$p['nome']}}</a></td>
                    <td>{{date('d/m/Y', strtotime($p['data_inizio']))}}</td>
                    <td>{{date('d/m/Y', strtotime($p['data_fine']))}}</td>
                    <td>€{{$p['costo_orario']}}</td>
                    <td>{{$hours[$i]}}h</td>
                    <td>€{{$p['costo_orario'] * $hours[$i]}}</td>
                </tr>
                @php $tot_hrs += $hours[$i]; $tot_mny += $p['costo_orario'] * $hours[$i++]; @endphp
            @endforeach
            @if($i > 1)
                <tr>
                    <th colspan="4">TOTALE</th>
                    <th>{{$tot_hrs}}h</th>
                    <th>€{{$tot_mny}}</th>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
    <div class="emptyspace">
    </div>
@stop
