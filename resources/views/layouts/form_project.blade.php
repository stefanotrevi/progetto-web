@include('layouts.form_inside',
        ['tag_id' => 'nome', 'type' => 'text', 'value' => $data['nome'] ?? '', 'label' => 'Nome'])

@include('layouts.form_inside',
        ['tag_id' => 'iva', 'type' => 'text', 'value' => $data['iva'] ?? '',
         'label' => 'P.IVA cliente'])

@isset($extended)
    @include('layouts.form_inside',
            ['tag_id' => 'descrizione', 'type' => 'text', 'value' => $data['descrizione'] ?? '',
             'label' => 'Descrizione', 'field' => 'textarea'])

    @include('layouts.form_inside',
            ['tag_id' => 'note', 'type' => 'text', 'value' => $data['note'] ?? '', 'label' => 'Note',
            'field' => 'textarea'])
@endisset

@include('layouts.form_inside',
        ['tag_id' => 'costo_orario', 'type' => 'number', 'value' => $data['costo_orario'] ?? '',
         'label' => 'Costo orario', 'step' => '0.01', 'min' => '0'])

@include('layouts.form_inside',
        ['tag_id' => 'data_inizio', 'type' => 'date', 'value' => $data['data_inizio'] ?? '',
         'label' => 'Data di inizio'])

@include('layouts.form_inside',
        ['tag_id' => 'data_fine', 'type' => 'date', 'value' => $data['data_fine'] ?? '',
         'label' => 'Data di fine'])
