@php
    $yield_form = $yield_form ?? 'NULL';
@endphp
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-body">
                @include('layouts.form_base')
            </div>
        </div>
    </div>
</div>
