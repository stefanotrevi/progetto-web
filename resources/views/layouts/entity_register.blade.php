@php
    $entity = $entity ?? 'NULL';
    $title = $title ?? 'NULL';
    $route = 'register' . ($entity == 'user' ? '' : '_' . $entity);
    $yield_form = 'form_' . $entity;
    $extended = '';
@endphp
<div class="card">
    <div class="card-header">
        <h2 class="mb-0">
            <button class="btn btn-link" type="button" data-toggle="collapse"
                    data-target="#{{ $route }}" aria-expanded="false"
                    aria-controls="{{ $route }}">
                {{$title}}
            </button>
        </h2>
    </div>
    <div id="{{ $route }}" class="collapse">
        @include('layouts.form_outside')
    </div>
</div>

