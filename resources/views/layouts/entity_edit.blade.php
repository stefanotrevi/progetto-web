@php
    $entity = $entity ?? 'NULL';
    $id = $id ?? 'NULL';
    $colspan = count($data);
@endphp
<tr>
    <td colspan="{{$colspan}}" class="p-0">
        <div id="{{'edit_' . $entity . $id}}" class="collapse">
            @include('layouts.form_outside')
        </div>
    </td>
</tr>
