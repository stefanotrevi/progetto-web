@php
    $tag_id = $tag_id ?? '';
    $value = $value ?? '';
    $type = $type ?? '';
    $label = $label ?? '';
    $min = $min ?? '';
    $step = $step ?? '';
    $field = $field ?? '';
    $class = $class ?? '';
    $i = 0;

@endphp

<div class="form-group row">
    <label for="{{$tag_id}}" class="col-md-4 col-form-label text-md-right">
        {{$label}}
    </label>
    <div class="col-md-6">
        {{-- se isset($textarea) è vero allora aggiunge il paragrafo allungabile(es. note) --}}
        @if($field == 'textarea')
            <textarea id="{{$tag_id}}" type="{{$type}}" min="{{$min}}" step="{{$step}}"
                      class="form-control @error($tag_id) is-invalid @enderror" name="{{$tag_id}}"
                      required autocomplete="{{$tag_id}}">{{$value}}</textarea>
            {{-- se invece isset($select) è vero allora aggiunge il form select (es.: progetti selezionabili in dashoboard)--}}
        @elseif($field == 'select')
            {!! Form::select($tag_id, $value->pluck('nome'), $value->pluck('id'), ['class' => 'custom-select']) !!}
        @else {{-- altrimenti mette un capo di input --}}
        <input id="{{$tag_id}}" type="{{$type}}" min="{{$min}}" step="{{$step}}"
               class="form-control {{$class}} @error($tag_id) is-invalid @enderror" name="{{$tag_id}}"
               value="{{$value}}" required autocomplete="{{$tag_id}}">
        @endif
        @error($tag_id)
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
