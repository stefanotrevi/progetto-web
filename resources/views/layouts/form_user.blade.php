@include('layouts.form_inside',
        ['tag_id' => 'name', 'type' => 'text', 'value' => $data['name'] ?? '', 'label' => 'Nome'])

@include('layouts.form_inside',
        ['tag_id' => 'email', 'type' => 'email', 'value' => $data['email'] ?? '', 'label' => 'E-Mail'])

<div class="form-group">
    <div class="col-md-6 offset-md-4">
        <div class="form-check">
            <input class="form-check-input pull-left" type="checkbox" value="Admin"
                   id="role"
                   name="role">
            <label class="form-check-label" for="role">
                Admin
            </label>
        </div>
    </div>
</div>
