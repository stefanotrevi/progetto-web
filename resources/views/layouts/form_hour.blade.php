@include('layouts.form_inside',
        ['tag_id' => 'data', 'type' => 'date', 'value' => $data['data'] ?? '',
         'label' => 'Data'])

@include('layouts.form_inside',
    ['tag_id' => 'project_id', 'value' => $data['projects'] ?? [],
    'label' => 'Progetto', 'field'=>'select'])

@include('layouts.form_inside',
        ['tag_id'=>'ore', 'type'=>'number', 'value'=> $data['ore'] ?? '', 'step' => '1',
         'min' => '0', 'label'=>'Ore'])

@include('layouts.form_inside',
    ['tag_id' => 'note', 'type' => 'text', 'value' => $data['note'] ?? '', 'label' => 'Note',
    'field' => 'textarea'])