@php
    $hint = $hint ?? 'Cerca qualcosa';
    $tag_id = $tag_id ?? 'elementnotexists';
@endphp
<div class="col">
    <form class="form-inline mb-2">
        <input class="form-control mr-sm-2" type="search" id="{{$tag_id}}"
               placeholder="{{$hint}}"
               aria-label="Cerca">
    </form>
</div>