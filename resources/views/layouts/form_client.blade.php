@include('layouts.form_inside',
        ['tag_id' => 'ragione', 'type' => 'text', 'value' => $data['ragione'] ?? '',
         'label' => 'Ragione'])

@include('layouts.form_inside',
        ['tag_id' => 'referente', 'type' => 'text', 'value' => $data['referente'] ?? '',
         'label' => 'Nome Referente'])

@include('layouts.form_inside',
        ['tag_id' => 'email', 'type' => 'email', 'value' => $data['email'] ?? '',
         'label' => 'E-mail referente'])

@include('layouts.form_inside',
        ['tag_id' =>'ssid', 'type' => 'text', 'value' => $data['ssid'] ?? '', 'label' => 'SSID'])

@include('layouts.form_inside',
        ['tag_id' => 'pec', 'type' => 'email', 'value' => $data['pec'] ?? '',
         'label' => 'PEC referente'])

@include('layouts.form_inside',
        ['tag_id' => 'iva', 'type' => 'text', 'value' => $data['iva'] ?? '', 'label' => 'P. IVA'])
