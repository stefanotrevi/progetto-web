@php
    $entity = $entity ?? 'NULL';
    $id = $id ?? 'NULL';
    $tag_id = 'edit_' . $entity . $id;
@endphp
<tr class="hl-on-hover"
    data-toggle="collapse" data-target="#{{$tag_id}}"
    aria-expanded="false" aria-controls="{{$tag_id}}">
    @foreach($data as $el)
        <td>{{$el}}</td>
    @endforeach
</tr>
