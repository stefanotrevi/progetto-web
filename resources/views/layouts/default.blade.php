<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layouts.head')
    <link rel="stylesheet" href="css/main.css"/>
</head>
<body>
<header id="header">
    @include('layouts.header')
</header>
@yield('content')
<footer id="footer">
    @include('layouts.footer')
</footer>
</body>
</html>
