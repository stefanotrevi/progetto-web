<div class="inner">
    <a href={{url('/')}} class="logo"><strong>BusinessTrack</strong></a>
    <nav id="nav">
        @auth
            <a href={{route('dashboard', ['start' => date('Y-m-01', strtotime(now())),
                                          'end' => date('Y-m-t', strtotime(now()))])}}>
                Dashboard</a>
        @else
            <a href={{route('login')}}>Login</a>
        @endauth
    </nav>
    <a href="#navPanel" class="navPanelToggle"><span class="fa fa-bars"></span></a>
</div>
