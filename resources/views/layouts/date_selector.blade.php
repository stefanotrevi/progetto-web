@php
    $id = $id ?? '';
    $route = $route ?? '';
@endphp

<div class="row mb-4 mt-5">
    <form class="form-inline col-md-12" method="GET" action="{{ route($route, ['id' => $id]) }}">
        <div class="offset-md-2 col-md-2">
            @include('layouts.form_inside',
                    ['tag_id' => 'start', 'type' => 'date', 'value' => Request::query('start'),
                    'label' => 'Inizio'])
        </div>
        <div class="offset-md-2 col-md-2">
            @include('layouts.form_inside',
                    ['tag_id' => 'end', 'type' => 'date', 'value' => Request::query('end'),
                     'label' => 'Fine'])
        </div>
        <div class="offset-md-2 col-md-2">
            <button type="submit" class="btn btn-primary col-md-12">
                Visualizza
            </button>
        </div>
    </form>
</div>
