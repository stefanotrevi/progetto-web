@include('layouts.form_inside',
        ['tag_id' => 'email', 'type' => 'email', 'value' => $data['email'] ?? '', 'label' => 'E-Mail utente'])

@include('layouts.form_inside',
        ['tag_id' => 'project_id', 'type' => 'text', 'value' => $id ?? '', 'class' => 'collapse'])
