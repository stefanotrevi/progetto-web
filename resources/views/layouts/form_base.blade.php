@php
    $id = $id ?? '';
    $entity = $entity ?? '';
    $kind = $kind ?? '';
@endphp
@if($kind == 'edit' || $kind == 'edit_details')
    <form method="POST" action="{{ route('edit_' . $entity, ['id' => $id]) }}">
        @csrf
        @method('PATCH')
        @include('layouts.form_' . $entity)
        <div class="row mb-4">
            @if($kind == 'edit_details')
                <div class="col-md-4 offset-md-4">
                    <a class="btn btn-info" href={{route($entity, ['id' => $id,
                    'start' => date('Y-m-01', strtotime(now())),
                    'end' => date('Y-m-t', strtotime(now()))])}}>
                        Dettagli</a>
                </div>
                @php  $btn_class = 'col-md-2'; $btn_style = ''; @endphp
            @else
                @php  $btn_class = 'mt-2 col-md-6 offset-md-4'; $btn_style = 'col-md-12'; @endphp
            @endif
            <div class="{{$btn_class}}">
                <button type="submit" class="btn btn-primary {{$btn_style}} ">
                    Modifica
                </button>
            </div>
        </div>
    </form>
    <form method="POST" action="{{ route('delete_' . $entity, ['id' => $id]) }}">
        @csrf
        @method('DELETE')
        <div class="row">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-outline-danger col-md-12">
                    Elimina
                </button>
            </div>
        </div>
    </form>
@else
    <form method="POST" action="{{ route('register_' . $entity)}}">
        @csrf
        @include('layouts.form_' . $entity)
        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{strtok($title, " ")}}
                </button>
            </div>
        </div>
    </form>
@endif

@if(isset($errors) && $errors->first('entity') == $entity)
    <div class="mt-2">
        @foreach($errors->all() as $e)
            @if($e != $entity)
                <div class="alert alert-danger">{{$e}}</div>
            @endif
        @endforeach
    </div>
@endif