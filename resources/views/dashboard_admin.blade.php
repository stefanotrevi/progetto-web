@extends('layouts.app')
@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
    <script src="{{asset('js/search.js')}}"></script>
@stop
@section('content')
    <div class="container mt-5">
        <div class="text-center mb-4">
            <a class="btn btn-primary text-center" role="button" href={{route('dashboard')}}>
                Dashboard
            </a>
        </div>

        <!-- Tendina di registrazione-->
        <div class="accordion mb-5 mt-4">
            @include('layouts.entity_register', ['entity' => 'user', 'title' => 'Registra un nuovo utente'])

            @include('layouts.entity_register', ['entity' => 'client', 'title' => 'Aggiungi un nuovo cliente'])

            @include('layouts.entity_register', ['entity' => 'project', 'title' => 'Crea un nuovo progetto'])
        </div>
        <!-- Caselle di ricerca-->
        <div class="row mt-4">
            @include('layouts.entity_search', ['tag_id' => 'searchUser', 'hint' => 'utente'])
            @include('layouts.entity_search', ['tag_id' => 'searchClient', 'hint' => 'cliente'])
            @include('layouts.entity_search', ['tag_id' => 'searchProject', 'hint' => 'progetto'])
        </div>

        <!-- Tabella dei risultati-->
        <div class="table-responsive mt-5">
            <table class="table table-striped table-bordered">
                <thead class="thead-dark"></thead>
                <tbody class="accordion mb-5 mt-4"></tbody>
            </table>
        </div>
    </div>
@stop
