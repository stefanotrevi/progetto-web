@php
    $hours = $hours ?? [];
    $tot_hrs = $tot_mny = 0;
@endphp
@extends('layouts.app')
@section('content')
    <div class="container mt-5">
        @if(Auth::user()->isAdmin())
            <div class="text-center">
                <a class="btn btn-primary text-center" role="button" href={{route('dashboard_admin')}}>
                    Admin dashboard
                </a>
            </div>
        @endif
        @include('layouts.date_selector', ['route' => 'dashboard'])
        <table class="table table-striped table-bordered">
            <thead class="thead-dark">
            <tr>
                <th>Data</th>
                <th>Progetto</th>
                <th>Note</th>
                <th>Ore</th>
                <th>Compenso</th>
            </tr>
            </thead>
            <tbody>
            @foreach($hours as $h)
                <tr>
                    <td>{{$h['data']}}</td>
                    <td>{{$h->project['nome']}}</td>
                    <td>{{$h['note']}}</td>
                    <td>{{$h['ore']}}h</td>
                    <td>€{{$h['ore'] * $h->project['costo_orario']}}</td>
                </tr>
                @php $tot_hrs += $h['ore']; $tot_mny += $h['ore'] * $h->project['costo_orario']; @endphp
            @endforeach
            <tr>
                <th colspan="3">Totale</th>
                <th>{{$tot_hrs}}h</th>
                <th>€{{$tot_mny}}</th>
            </tr>
            </tbody>
        </table>

        @include('layouts.entity_register', ['entity' => 'hour', 'title' => 'Inserisci nuova scheda ore'])
    </div>
@stop