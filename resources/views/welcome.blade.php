@extends('layouts.default')
@section('content')
    <section id="banner">
        <div class="inner">
            <header>
                <h1>BusinessTrack</h1>
                <h4>Keep an eye on your performance!</h4>
            </header>

            <div class="flex ">

                <div>
                    <span class="icon fa-lightbulb-o"></span>
                    <h3>Discover</h3>
                    <p>our state-of-the-art tools</p>
                </div>

                <div>
                    <span class="icon fa-pie-chart"></span>
                    <h3>Inspect</h3>
                    <p>your company's daily digests</p>
                </div>

                <div>
                    <span class="icon fa-line-chart"></span>
                    <h3>Achieve</h3>
                    <p>optimal business results</p>
                </div>

            </div>

            <footer>
                <a href="#footer" class="button">START NOW</a>
            </footer>
        </div>
    </section>


    <!-- Three -->
    <section id="three" class="wrapper align-center">
        <div class="inner">
            <div class="flex flex-2">
                <article>
                    <div class="image round">
                        <img src="images/pic01.jpg" alt="Pic 01"/>
                    </div>
                    <header>
                        <h3>Lorenzo Cervi</h3>
                    </header>
                    <p>Lorenz Ipsum... Classe 1998, studente al terzo anno di informatica <br>
                       presso l'università di Ferrara, <strong>co-fondatore</strong> del progetto
                        <br><br><br></p>
                    <footer>
                        <a href="#" class="button">Learn More</a>
                    </footer>
                </article>
                <article>
                    <div class="image round">
                        <img src="images/pic02.jpg" alt="Pic 02"/>
                    </div>
                    <header>
                        <h3>Stefano Trevisani</h3>
                    </header>
                    <p>Classe 1998, studente al terzo anno di informatica presso<br>
                       l'università di Ferrara, <strong>co-fondatore</strong> del progetto.<br>
                       Appassionato di <strong>tecnologia</strong>, gaming ed <strong>HiFi</strong>
                       ama la musica<br>
                       dei Bastille, ed è un fan sfegatato di <strong>Guerre Stellari.</strong>
                    </p>
                    <footer>
                        <a href="#" class="button">Learn More</a>
                    </footer>
                </article>
            </div>
        </div>
    </section>

    <!-- Scripts -->
    <script src="js/jquery.min.js"></script>
    <script src="js/skel.min.js"></script>
    <script src="js/util.js"></script>
    <script src="js/main.js"></script>
@stop
