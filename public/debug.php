<tr class="hl-on-hover"
    data-toggle="collapse" data-target="#edit_user2"
    aria-expanded="false" aria-controls="edit_user2">
            <td>Lorenzo</td>
            <td>lore.cervi@gmail.com</td>
            <td>Admin</td>
            <td>01/01/1970</td>
    </tr>
<tr>
    <td colspan="4" class="p-0">
        <div id="edit_user2" class="collapse">
            <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-body">
                <form method="POST" action="http://businesstrack.test/users/2/edit">
        <input type="hidden" name="_token" value="K2Mahevv6UsqpHRY1OZFUkhY5Hno71MVn1QLjdwr">        <input type="hidden" name="_method" value="PATCH">        <div class="form-group row">
    <label for="name" class="col-md-4 col-form-label text-md-right">
        Nome
    </label>
    <div class="col-md-6">
        
         
        <input id="name" type="text" min="" step=""
               class="form-control  " name="name"
               value="Lorenzo" required autocomplete="name">
                    </div>
</div>

<div class="form-group row">
    <label for="email" class="col-md-4 col-form-label text-md-right">
        E-Mail
    </label>
    <div class="col-md-6">
        
         
        <input id="email" type="email" min="" step=""
               class="form-control  " name="email"
               value="lore.cervi@gmail.com" required autocomplete="email">
                    </div>
</div>

<div class="form-group">
    <div class="col-md-6 offset-md-4">
        <div class="form-check">
            <input class="form-check-input pull-left" type="checkbox" value="Admin"
                   id="role"
                   name="role">
            <label class="form-check-label" for="role">
                Admin
            </label>
        </div>
    </div>
</div>
        <div class="row mb-4">
                                                    <div class="mt-2 col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary col-md-12 ">
                    Modifica
                </button>
            </div>
        </div>
    </form>
    <form method="POST" action="http://businesstrack.test/users/2/delete">
        <input type="hidden" name="_token" value="K2Mahevv6UsqpHRY1OZFUkhY5Hno71MVn1QLjdwr">        <input type="hidden" name="_method" value="DELETE">        <div class="row">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-outline-danger col-md-12">
                    Elimina
                </button>
            </div>
        </div>
    </form>

            </div>
        </div>
    </div>
</div>
        </div>
    </td>
</tr>
<tr class="hl-on-hover"
    data-toggle="collapse" data-target="#edit_user8"
    aria-expanded="false" aria-controls="edit_user8">
            <td>Siro Silvestri</td>
            <td>dangelo.albino@example.net</td>
            <td>User</td>
            <td>26/01/2016</td>
    </tr>
<tr>
    <td colspan="4" class="p-0">
        <div id="edit_user8" class="collapse">
            <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-body">
                <form method="POST" action="http://businesstrack.test/users/8/edit">
        <input type="hidden" name="_token" value="K2Mahevv6UsqpHRY1OZFUkhY5Hno71MVn1QLjdwr">        <input type="hidden" name="_method" value="PATCH">        <div class="form-group row">
    <label for="name" class="col-md-4 col-form-label text-md-right">
        Nome
    </label>
    <div class="col-md-6">
        
         
        <input id="name" type="text" min="" step=""
               class="form-control  " name="name"
               value="Siro Silvestri" required autocomplete="name">
                    </div>
</div>

<div class="form-group row">
    <label for="email" class="col-md-4 col-form-label text-md-right">
        E-Mail
    </label>
    <div class="col-md-6">
        
         
        <input id="email" type="email" min="" step=""
               class="form-control  " name="email"
               value="dangelo.albino@example.net" required autocomplete="email">
                    </div>
</div>

<div class="form-group">
    <div class="col-md-6 offset-md-4">
        <div class="form-check">
            <input class="form-check-input pull-left" type="checkbox" value="Admin"
                   id="role"
                   name="role">
            <label class="form-check-label" for="role">
                Admin
            </label>
        </div>
    </div>
</div>
        <div class="row mb-4">
                                                    <div class="mt-2 col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary col-md-12 ">
                    Modifica
                </button>
            </div>
        </div>
    </form>
    <form method="POST" action="http://businesstrack.test/users/8/delete">
        <input type="hidden" name="_token" value="K2Mahevv6UsqpHRY1OZFUkhY5Hno71MVn1QLjdwr">        <input type="hidden" name="_method" value="DELETE">        <div class="row">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-outline-danger col-md-12">
                    Elimina
                </button>
            </div>
        </div>
    </form>

            </div>
        </div>
    </div>
</div>
        </div>
    </td>
</tr>
<tr class="hl-on-hover"
    data-toggle="collapse" data-target="#edit_user10"
    aria-expanded="false" aria-controls="edit_user10">
            <td>Terzo Colombo</td>
            <td>raoul.rossetti@example.net</td>
            <td>User</td>
            <td>25/04/2015</td>
    </tr>
<tr>
    <td colspan="4" class="p-0">
        <div id="edit_user10" class="collapse">
            <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-body">
                <form method="POST" action="http://businesstrack.test/users/10/edit">
        <input type="hidden" name="_token" value="K2Mahevv6UsqpHRY1OZFUkhY5Hno71MVn1QLjdwr">        <input type="hidden" name="_method" value="PATCH">        <div class="form-group row">
    <label for="name" class="col-md-4 col-form-label text-md-right">
        Nome
    </label>
    <div class="col-md-6">
        
         
        <input id="name" type="text" min="" step=""
               class="form-control  " name="name"
               value="Terzo Colombo" required autocomplete="name">
                    </div>
</div>

<div class="form-group row">
    <label for="email" class="col-md-4 col-form-label text-md-right">
        E-Mail
    </label>
    <div class="col-md-6">
        
         
        <input id="email" type="email" min="" step=""
               class="form-control  " name="email"
               value="raoul.rossetti@example.net" required autocomplete="email">
                    </div>
</div>

<div class="form-group">
    <div class="col-md-6 offset-md-4">
        <div class="form-check">
            <input class="form-check-input pull-left" type="checkbox" value="Admin"
                   id="role"
                   name="role">
            <label class="form-check-label" for="role">
                Admin
            </label>
        </div>
    </div>
</div>
        <div class="row mb-4">
                                                    <div class="mt-2 col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary col-md-12 ">
                    Modifica
                </button>
            </div>
        </div>
    </form>
    <form method="POST" action="http://businesstrack.test/users/10/delete">
        <input type="hidden" name="_token" value="K2Mahevv6UsqpHRY1OZFUkhY5Hno71MVn1QLjdwr">        <input type="hidden" name="_method" value="DELETE">        <div class="row">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-outline-danger col-md-12">
                    Elimina
                </button>
            </div>
        </div>
    </form>

            </div>
        </div>
    </div>
</div>
        </div>
    </td>
</tr>
