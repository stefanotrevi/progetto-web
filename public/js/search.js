$(document).ready(function ()
{
    fetch_data();

    function fetch_data(url, query, place)
    {
        $.ajax({
            url: url,
            method: 'GET',
            data: {query: query},
            dataType: 'json',
            success: function (data)
            {
                $(place).html(data).fadeIn();
            },
            error: function (data)
            {
                $(place).html(data).fadeOut();
            }

        })
    }

    // Client
    $(document).on('keyup', '#searchClient', function ()
    {
        fetch_data('/clients/search', $(this).val(), 'tbody');
    });

    $(document).on('click', '#searchClient', function ()
    {
        // cambio contesto
        fetch_data('/clients/search', $(this).val(), 'tbody');
        fetch_data('/clients/layout', null, 'thead');
    });
    
    // User
    $(document).on('keyup', '#searchUser', function ()
    {
        fetch_data('/users/search', $(this).val(), 'tbody');
    });

    $(document).on('click', '#searchUser', function ()
    {
        fetch_data('/users/search', $(this).val(), 'tbody');
        fetch_data('/users/layout', null, 'thead');
    });


    // Projects
    $(document).on('keyup', '#searchProject', function ()
    {
     fetch_data('/projects/search', $(this).val(), 'tbody');
    });
 
    $(document).on('click', '#searchProject', function ()
    {
        fetch_data('/projects/search', $(this).val(), 'tbody');
        fetch_data('/projects/layout', null, 'thead');
    });

});
