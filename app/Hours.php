<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hours extends Model
{
    protected $fillable = ['user_id', 'project_id', 'data', 'ore', 'note'];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}
