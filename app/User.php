<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'date:d/m/Y',
    ];

    public function isAdmin()
    {
        return $this->role == 'Admin';
    }

    public function projects()
    {
        return $this->hasManyThrough(Project::class, ProjectUser::class,
                                     'user_id', 'id',
                                     'id', 'project_id');
    }

    public function hours()
    {
        return $this->hasMany(Hours::class)->orderBy('data');
    }

}
