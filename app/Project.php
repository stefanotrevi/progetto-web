<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = ['nome', 'client_id', 'descrizione', 'note',
                           'costo_orario', 'data_inizio', 'data_fine'];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function hours()
    {
        return $this->hasMany(Hours::class);
    }
}
