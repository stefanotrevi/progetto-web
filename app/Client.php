<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['ragione', 'referente', 'email', 'ssid', 'pec', 'iva'];

    public function projects()
    {
        return $this->hasMany(Project::class);
    }
}
