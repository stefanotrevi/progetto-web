<?php

namespace App\Http\Controllers;

use App\ProjectUser;
use App\User;
use Illuminate\Http\Request;
use Validator;

class ProjectUserController extends Controller
{
    public function create()
    {
    }

    public function store(Request $request)
    {

        // sostituisco all'email l'id
        $request['user_id'] = User::query()
                                  ->where('email', '=', $request['email'])
                                  ->first()['id'];

        $input = $request->all();
        $validator = Validator::make($input, [
            'user_id' => 'required|integer|exists:users,id',
            'project_id' => 'required|integer|exists:projects,id',
        ]);

        if ($validator->fails())
        {
            $validator->getMessageBag()->add('entity', 'projectuser');
            return redirect()->back()->withErrors($validator)->withInput();
        }
        ProjectUser::create(request()->all());

        return redirect()->back();

    }

}
