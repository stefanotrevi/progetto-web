<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Validator;

class UserController extends Controller
{
    /**
     * @param Request $request
     * @throws \Throwable
     */
    public function index(Request $request)
    {
        if (!$request->ajax())
            return;

        $query = '%' . $request->get('query') . '%';
        $data = $query == '%%' ? [] : User::query()
            ->where('name', 'like', $query)
            ->orWhere('email', 'like', $query)
            ->orWhere('role', 'like', $query)
            ->orderBy('name', 'asc')
            ->get();

        $output = '';
        if ($data->count() > 0)
            foreach ($data as $row)
                $output .= $this->snippet($row) . $this->edit($row);

        file_put_contents('debug.php', $output);
        echo json_encode($output);
    }

    /**
     * @param User $user
     * @return array|string
     */
    public function snippet(User $user)
    {
        try
        {
            $view = view('layouts.entity_snippet', [
                'entity' => 'user', 'id' => $user['id'], 'data' => [
                    'name' => $user['name'],
                    'email' => $user['email'],
                    'role' => $user['role'],
                    'email_verified_at' => date('d/m/Y', strtotime($user['email_verified_at'])),
                ]])->render();
        } catch (\Throwable $e)
        {
            file_put_contents('debug.php', $e);
        }

        return $view;
    }

    /**
     * @param User $user
     * @return array|string
     */
    public function edit(User $user)
    {

        try
        {
            return view('layouts.entity_edit', [
                'entity' => 'user', 'id' => $user['id'], 'kind' => 'edit', 'data' => [
                    'name' => $user['name'],
                    'email' => $user['email'],
                    'role' => $user['role'],
                    'email_verified_at' => $user['email_verified_at'],
                ]])->render();
        } catch (\Throwable $e)
        {
            file_put_contents('debug.php', $e);
        }
    }

    /**
     * @param User $user
     * @return array|string
     * @throws \Throwable
     */
    public function show(User $user)
    {
        return view('user', [
            'entity' => 'user', 'id' => $user['id'],
            'data' => [
                'name' => $user['name'],
                'email' => $user['email'],
                'role' => $user['role'],
                'email_verified_at' => date('d/m/Y', strtotime($user['email_verified_at'])),
            ]])->render();
    }

    public function layout()
    {
        $layout =
            '<tr>
                <th>Nome</th>
                <th>Email</th>
                <th>Ruolo</th>
                <th>Data iscrizione</th>
            </tr>';

        echo json_encode($layout);
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required',
            'email' => 'required|email|unique:' . (new User())->getTable() . ',email,' . $id,
        ]);

        if ($validator->fails())
        {
            $validator->getMessageBag()->add('entity', 'user');
            return redirect()->back()->withErrors($validator)->withInput();
        }
        User::query()->find($id)->fill($input)->save();

        return redirect()->back();
    }

    public function destroy($id)
    {
        User::query()->find($id)->delete();

        return redirect()->back();
    }
}
