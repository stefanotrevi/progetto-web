<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $user = Auth::user();

        return view('dashboard', [
            'hours' => $user->hours
                ->whereBetween('data', [$request->query('start'), $request->query('end')]),
            'data' => [
                'data' => now(),
                'projects' => $user->projects,
                'ore' => now(),
            ]
        ]);
    }
}
