<?php

namespace App\Http\Controllers;

use App\Hours;
use Illuminate\Http\Request;
use Validator;

use Auth;

class HourController extends Controller
{

    public function create()
    {

    }

    public function store(Request $request)
    {
        $request['user_id'] = Auth::user()['id'];
        $input = $request->all();
        $validator = Validator::make($input, [
            'user_id' => 'required|integer|exists:users,id',
            'project_id' => 'required|integer|exists:projects,id',
            'data' => 'required|date',
            'ore' => 'required',
        ]);

        if ($validator->fails())
        {
            $validator->getMessageBag()->add('entity', 'hours');
            return redirect()->back()->withErrors($validator)->withInput();
        }

        Hours::create($input);

        return redirect()->back();
    }
}
