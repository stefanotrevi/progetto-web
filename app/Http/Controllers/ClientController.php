<?php

namespace App\Http\Controllers;

use App\Client;
use App\Hours;
use App\Project;
use Illuminate\Http\Request;
use Validator;

class ClientController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return void
     * @throws \Throwable
     */
    public function index(Request $request)
    {
        if (!$request->ajax())
            return;

        $query = '%' . $request->get('query') . '%';
        $data = $query == '%%' ? [] : Client::query()
            ->where('ragione', 'like', $query)
            ->orWhere('iva', 'like', $query)
            ->orWhere('referente', 'like', $query)
            ->orderBy('ragione', 'asc')
            ->get();

        $output = '';
        if ($data->count() > 0)
            foreach ($data as $row)
                $output .= $this->snippet($row) . $this->edit($row);

        echo json_encode($output);
    }

    /**
     * Display a snippet of the specified resource.
     *
     * @param \App\Client $client
     * @return array|string
     * @throws \Throwable
     */
    public function snippet(Client $client)
    {
        return view('layouts.entity_snippet', [
            'entity' => 'client',
            'id' => $client['id'],
            'data' => [
                'ragione' => $client['ragione'],
                'referente' => $client['referente'],
                'email' => $client['email'],
                'ssid' => $client['ssid'],
                'pec' => $client['pec'],
                'iva' => $client['iva'],
            ]])->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Client $client
     * @return array|string
     * @throws \Throwable
     */
    public function edit(Client $client)
    {

        return view('layouts.entity_edit', [
            'entity' => 'client',
            'id' => $client['id'],
            'kind' => 'edit_details',
            'data' => [
                'ragione' => $client['ragione'],
                'referente' => $client['referente'],
                'email' => $client['email'],
                'ssid' => $client['ssid'],
                'pec' => $client['pec'],
                'iva' => $client['iva'],
            ]])->render();
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @param Request $request
     * @return array|string
     */
    public function show($id, Request $request)
    {
        $client = Client::query()->find($id);
        $start = $request->query('start');
        $end = $request->query('end');
        $projects = $client->projects->where('data_inizio', '<', $end)->where('data_fine', '>', $start);
        $hours = [];
        $i = 0;
        foreach ($projects as $p)
            $hours[$i++] = $p->hours->whereBetween('data', [$start, $end])->sum('ore');

        return view('client', [
            'id' => $client['id'], 'projects' => $projects, 'hours' => $hours,
            'data' => [
                'ragione' => $client['ragione'],
                'referente' => $client['referente'],
                'email' => $client['email'],
                'ssid' => $client['ssid'],
                'pec' => $client['pec'],
                'iva' => $client['iva'],
            ]]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'ragione' => 'required',
            'referente' => 'required',
            'email' => 'required|email',
            'ssid' => 'required|alpha_num',
            'pec' => 'required|email',
            'iva' => 'required|alpha_num|unique:' . (new Client)->getTable() . ',iva',
        ]);

        if ($validator->fails())
        {
            $validator->getMessageBag()->add('entity', 'client');
            return redirect()->back()->withErrors($validator)->withInput();
        }

        Client::create($input);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Client $client
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        Client::query()->find($id)->delete();

        return redirect()->back();
    }

    public function layout()
    {
        $layout =
            '<tr>
                <th>Ragione</th>
                <th>Referente</th>
                <th>Email</th>
                <th>SSID</th>
                <th>PEC</th>
                <th>P. IVA</th>
            </tr>';

        echo json_encode($layout);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'ragione' => 'required',
            'referente' => 'required',
            'email' => 'required|email',
            'ssid' => 'required|alpha_num',
            'pec' => 'required|email',
            'iva' => 'required|alpha_num|unique:' . (new Client)->getTable() . ',iva,' . $id,
        ]);

        if ($validator->fails())
        {
            $validator->getMessageBag()->add('entity', 'client');
            return redirect()->back()->withErrors($validator)->withInput();
        }
        Client::query()->find($id)->fill($input)->save();

        return redirect()->back();
    }
}
