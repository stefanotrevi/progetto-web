<?php

namespace App\Http\Controllers;

use App\Client;
use App\Project;
use App\User;
use Eloquent;
use Illuminate\Http\Request;
use Validator;

/**
 * Class ProjectController
 * @package App\Http\Controllers
 * @mixin Eloquent
 */
class ProjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return void
     * @throws \Throwable
     */
    public function index(Request $request)
    {
        if (!$request->ajax())
            return;

        $ctable = (new Client)->getTable();
        $query = '%' . $request->get('query') . '%';
        $data = $query == '%%' ? [] : Project::query()
            ->select((new Project())->getTable() . '.*')
            ->join($ctable, $ctable . '.id', '=', 'client_id')
            ->where('nome', 'like', $query)
            ->orWhere('data_inizio', 'like', $query)
            ->orWhere('data_fine', 'like', $query)
            ->orWhere('iva', 'like', $query)
            ->orderBy('data_inizio', 'asc')
            ->get();

        $output = '';
        if ($data->count() > 0)
            foreach ($data as $row)
                $output .= $this->snippet($row) . $this->edit($row);

        echo json_encode($output);
    }

    /**
     * Shows a snippet of the specified resource.
     *
     * @param \App\Project $project
     * @return array|string
     * @throws \Throwable
     */
    public function snippet(Project $project)
    {
        return view('layouts.entity_snippet', [
            'entity' => 'project', 'id' => $project['id'], 'data' => [
                'nome' => $project['nome'],
                'iva' => $project->client['iva'],
                'costo_orario' => '€' . $project['costo_orario'],
                'data_inizio' => date('d/m/Y', strtotime($project['data_inizio'])),
                'data_fine' => date('d/m/Y', strtotime($project['data_fine'])),
            ]])->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Project $project
     * @return array|string
     * @throws \Throwable
     */
    public function edit(Project $project)
    {
        return view('layouts.entity_edit', [
            'entity' => 'project', 'id' => $project['id'], 'kind' => 'edit_details', 'data' => [
                'nome' => $project['nome'],
                'iva' => $project->client['iva'],
                'costo_orario' => $project['costo_orario'],
                'data_inizio' => $project['data_inizio'],
                'data_fine' => $project['data_fine'],
            ]])->render();
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @param Request $request
     * @return array|string
     */
    public function show($id, Request $request)
    {
        $project = Project::query()->find($id);
        $start = $request->query('start');
        $end = $request->query('end');
        $users = $project->users;
        $hours = $prest = [];
        $i = 0;

        foreach ($users as $u)
        {
            $temp = $u->hours->where('project_id', '=', $id)
                ->whereBetween('data', [$start, $end]);
            $prest[$i]['first'] = $temp->first()['data'] ?? '';
            $prest[$i]['last'] = $temp->last()['data'] ?? '';
            $hours[$i++] = $temp->sum('ore');
        }
        return view('project', [
            'id' => $project['id'], 'users' => $users, 'hours' => $hours, 'prest' => $prest,
            'data' => [
                'nome' => $project['nome'],
                'iva' => $project->client['iva'],
                'costo_orario' => $project['costo_orario'],
                'data_inizio' => $project['data_inizio'],
                'data_fine' => $project['data_fine'],
                'descrizione' => $project['descrizione'],
                'note' => $project['note'],
            ]]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        // sostituisco all'iva l'id
        $request['client_id'] = Client::query()
                                    ->where('iva', '=', $request['iva'])
                                    ->first()['id'] ?? NULL;
        $input = $request->all();
        $validator = Validator::make($input, [
            'nome' => 'required',
            'client_id' => 'exists:clients,id',
            'descrizione' => 'required',
            'note' => 'required',
            'costo_orario' => 'required',
            'data_inizio' => 'required|date',
            'data_fine' => 'required|date|after:data_inizio',
        ]);

        if ($validator->fails())
        {
            $validator->getMessageBag()->add('entity', 'project');
            return redirect()->back()->withErrors($validator)->withInput();
        }
        Project::create($input);

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'nome' => 'required',
            'client_id' => 'required|integer|exists:clients,id',
            'descrizione' => '',
            'note' => '',
            'costo_orario' => 'required|regex:^(?:[1-9]\d+|\d)(?:\,\d\d)?$',
            'data_inizio' => 'required|date',
            'data_fine' => 'required|date',

        ]);

        if ($validator->fails())
        {
            $validator->getMessageBag()->add('entity', 'project');
            return redirect('edit_project')->withErrors($validator)->withInput();
        }
        Project::query()->find($id)->fill($request->all())->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        Project::query()->find($id)->delete();

        return redirect()->back();
    }

    public function layout()
    {
        $layout =
            '<tr>
                <th>Nome</th>
                <th>Cliente</th>
                <th>Costo orario</th>
                <th>Data inizio</th>
                <th>Data fine</th>
            </tr>';

        echo json_encode($layout);
    }
}
